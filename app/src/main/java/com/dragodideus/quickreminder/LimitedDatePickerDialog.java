package com.dragodideus.quickreminder;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by rakifsul on 3/26/2016.
 */
public class LimitedDatePickerDialog extends DatePickerDialog {
    protected Calendar minDate;
    protected Calendar maxDate;

    public LimitedDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }

    public LimitedDatePickerDialog(Context context, int theme, OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, theme, listener, year, monthOfYear, dayOfMonth);
    }

    public Calendar getMinDate(){
        return minDate;
    }

    public Calendar getMaxDate(){
        return maxDate;
    }

    public void setRange(Calendar minDate, Calendar maxDate){
        this.minDate = minDate;
        this.maxDate = maxDate;
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int month, int day) {
        super.onDateChanged(view, year, month, day);

        if(minDate == null){
            return;
        }
        if(maxDate == null){
            return;
        }

        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(year,month,day);
        if(selectedDate.getTime().before(minDate.getTime())){
            updateDate(minDate.get(Calendar.YEAR), minDate.get(Calendar.MONTH), minDate.get(Calendar.DAY_OF_MONTH));
        } else if (selectedDate.getTime().after(maxDate.getTime())){
            updateDate(maxDate.get(Calendar.YEAR), maxDate.get(Calendar.MONTH), maxDate.get(Calendar.DAY_OF_MONTH));
        }

    }
}
