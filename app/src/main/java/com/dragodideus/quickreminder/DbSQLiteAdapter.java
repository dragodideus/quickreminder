package com.dragodideus.quickreminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by rakifsul on 3/24/2016.
 */
public class DbSQLiteAdapter {

    static class DbSQLiteHelper extends SQLiteOpenHelper{

        private static final String DATABASE_NAME = "QuickReminder";
        private static final int DATABASE_VERSION = 1;

        private static final String REMINDER_EVENT_TABLE_NAME = "reminder_event";
        private static final String REMINDER_EVENT_ID = "_id";
        private static final String REMINDER_EVENT_TITLE = "title";
        private static final String REMINDER_EVENT_DESCRIPTION = "description";
        private static final String REMINDER_EVENT_DATE = "date";
        private static final String REMINDER_EVENT_TIME = "time";
        private static final String REMINDER_EVENT_USES_ALARM = "uses_alarm";
        private static final String REMINDER_EVENT_RINGTONE_URI = "ringtone_uri";

        /*
        CREATE TABLE reminder_event (
        _id INTEGER PRIMARY KEY AUTOINCREMENT,
        title VARCHAR(255),
        description TEXT,
        date INTEGER,
        time INTEGER,
        uses_alarm INTEGER,
        ringtone_uri TEXT,
        status VARCHAR(10)
        );
        */
        private static final String REMINDER_EVENT_CREATE_TABLE =
                "CREATE TABLE " + REMINDER_EVENT_TABLE_NAME + "(" +
                        REMINDER_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        REMINDER_EVENT_TITLE + " VARCHAR(255), " +
                        REMINDER_EVENT_DESCRIPTION + " TEXT, " +
                        REMINDER_EVENT_DATE + " INTEGER, " +
                        REMINDER_EVENT_TIME + " VARCHAR(10), " +
                        REMINDER_EVENT_USES_ALARM + " INTEGER, " +
                        REMINDER_EVENT_RINGTONE_URI + " TEXT " +
                ");";

        private static final String REMINDER_EVENT_DROP_TABLE =
                "DROP TABLE IF EXISTS " + REMINDER_EVENT_TABLE_NAME;

        private Context context;

        //
        public DbSQLiteHelper(Context context){
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(REMINDER_EVENT_CREATE_TABLE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(REMINDER_EVENT_DROP_TABLE);
                onCreate(db);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //

    private DbSQLiteHelper dbSQLiteHelper;
    private Context context;

    //
    public DbSQLiteAdapter(Context context){
        dbSQLiteHelper = new DbSQLiteHelper(context);
        this.context = context;
    }

    public long insertReminderEvent(ReminderEvent reminderEvent){
        //
        if(reminderEvent == null){
            return -1;
        }

        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_TITLE, reminderEvent.getTitle());
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION, reminderEvent.getDescription());
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_DATE, Utils.dateAsLongCalendar(reminderEvent.getDate()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_TIME, Utils.timeAsStringCalendar(reminderEvent.getTime()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM, Utils.boolToInt(reminderEvent.getUsesAlarm()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI, reminderEvent.getRingtoneUri().toString());

        //
        return db.insert(DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME, null, contentValues);
    }

    public int deleteReminderEvent(int reminderEventID){
        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        String[] selectionArgs = {Integer.toString(reminderEventID)};

        //
        return db.delete(
                DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME,
                DbSQLiteHelper.REMINDER_EVENT_ID + "=?",
                selectionArgs);
    }

    public int updateReminderEvent(ReminderEvent reminderEvent){
        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_TITLE, reminderEvent.getTitle());
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION, reminderEvent.getDescription());
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_DATE, Utils.dateAsLongCalendar(reminderEvent.getDate()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_TIME, Utils.timeAsStringCalendar(reminderEvent.getTime()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM, Utils.boolToInt(reminderEvent.getUsesAlarm()));
        contentValues.put(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI, reminderEvent.getRingtoneUri().toString());

        //
        String[] selectionArgs = {Integer.toString(reminderEvent.getID())};

        return db.update(
                DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME,
                contentValues,
                DbSQLiteHelper.REMINDER_EVENT_ID + "=?",
                selectionArgs);
    }

    public ReminderEvent getReminderEvent(int reminderEventID){
        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        String[] columns = {
                DbSQLiteHelper.REMINDER_EVENT_ID,
                DbSQLiteHelper.REMINDER_EVENT_TITLE,
                DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION,
                DbSQLiteHelper.REMINDER_EVENT_DATE,
                DbSQLiteHelper.REMINDER_EVENT_TIME,
                DbSQLiteHelper.REMINDER_EVENT_USES_ALARM,
                DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI
        };

        //
        String[] selectionArgs = {Integer.toString(reminderEventID)};

        //
        Cursor cursor = db.query(
                DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME,
                columns,
                DbSQLiteHelper.REMINDER_EVENT_ID +"=?",
                selectionArgs,
                null, null, null);

        if(cursor == null){
            return null;
        }

        //
        cursor.moveToFirst();

        int id = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_ID));
        String title = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TITLE));
        String description = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION));
        int date = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DATE));
        String time = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TIME));
        int usesAlarm = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM));
        String ringtoneUri = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI));

        //
        ReminderEvent re = new ReminderEvent(
                id,
                title,
                description,
                Utils.longAsDateCalendar(date),
                Utils.stringAsTimeCalendar(time),
                Utils.intToBool(usesAlarm),
                Uri.parse(ringtoneUri)
        );

        return re;
    }

    public ArrayList<ReminderEvent> getReminderEventsAtDate(Calendar reminderEventDate){
        //
        ArrayList<ReminderEvent> reminderEvents = new ArrayList<ReminderEvent>();

        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        String[] columns = {
                DbSQLiteHelper.REMINDER_EVENT_ID,
                DbSQLiteHelper.REMINDER_EVENT_TITLE,
                DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION,
                DbSQLiteHelper.REMINDER_EVENT_DATE,
                DbSQLiteHelper.REMINDER_EVENT_TIME,
                DbSQLiteHelper.REMINDER_EVENT_USES_ALARM,
                DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI
        };

        //
        String[] selectionArgs = {Long.toString(Utils.dateAsLongCalendar(reminderEventDate))};

        //
        String orderBy =  DbSQLiteHelper.REMINDER_EVENT_TIME + " ASC";

        //
        Cursor cursor = db.query(
                DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME,
                columns,
                DbSQLiteHelper.REMINDER_EVENT_DATE +"=?",
                selectionArgs,
                null, null, orderBy);

        if(cursor == null){
            return null;
        }

        //
        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_ID));
            String title = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TITLE));
            String description = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION));
            int date = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DATE));
            String time = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TIME));
            int usesAlarm = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM));
            String ringtoneUri = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI));

            //
            ReminderEvent re = new ReminderEvent(
                    id,
                    title,
                    description,
                    Utils.longAsDateCalendar(date),
                    Utils.stringAsTimeCalendar(time),
                    Utils.intToBool(usesAlarm),
                    Uri.parse(ringtoneUri)
            );

            //
            reminderEvents.add(re);
        }

        //
        cursor.close();

        //
        return reminderEvents;
    }

    public ArrayList<ReminderEvent> getAllReminderEvents(){
        //
        ArrayList<ReminderEvent> reminderEvents = new ArrayList<ReminderEvent>();

        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        String[] columns = {
                DbSQLiteHelper.REMINDER_EVENT_ID,
                DbSQLiteHelper.REMINDER_EVENT_TITLE,
                DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION,
                DbSQLiteHelper.REMINDER_EVENT_DATE,
                DbSQLiteHelper.REMINDER_EVENT_TIME,
                DbSQLiteHelper.REMINDER_EVENT_USES_ALARM,
                DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI
        };

        //
        Cursor cursor = db.query(DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME, columns, null, null, null, null, null);

        if(cursor == null){
            return null;
        }

        //
        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_ID));
            String title = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TITLE));
            String description = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION));
            int date = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DATE));
            String time = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TIME));
            int usesAlarm = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM));
            String ringtoneUri = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI));

            //
            ReminderEvent re = new ReminderEvent(
                    id,
                    title,
                    description,
                    Utils.longAsDateCalendar(date),
                    Utils.stringAsTimeCalendar(time),
                    Utils.intToBool(usesAlarm),
                    Uri.parse(ringtoneUri)
            );

            //
            reminderEvents.add(re);
        }

        //
        cursor.close();

        //
        return reminderEvents;
    }

    public String debugShowAllReminderEvents(){
        //
        SQLiteDatabase db = dbSQLiteHelper.getWritableDatabase();

        //
        String[] columns = {
                DbSQLiteHelper.REMINDER_EVENT_ID,
                DbSQLiteHelper.REMINDER_EVENT_TITLE,
                DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION,
                DbSQLiteHelper.REMINDER_EVENT_DATE,
                DbSQLiteHelper.REMINDER_EVENT_TIME,
                DbSQLiteHelper.REMINDER_EVENT_USES_ALARM,
                DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI
        };

        //
        Cursor cursor = db.query(DbSQLiteHelper.REMINDER_EVENT_TABLE_NAME, columns, null, null, null, null, null);

        //
        StringBuffer stringBuffer = new StringBuffer();

        //
        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_ID));
            String title = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TITLE));
            String description = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DESCRIPTION));
            int date = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_DATE));
            String time = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_TIME));
            int usesAlarm = cursor.getInt(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_USES_ALARM));
            String ringtoneUri = cursor.getString(cursor.getColumnIndex(DbSQLiteHelper.REMINDER_EVENT_RINGTONE_URI));

            stringBuffer.append(id + "\n" +
                            title + "\n" +
                            description + "\n" +
                            date + "\n" +
                            time + "\n" +
                            usesAlarm + "\n" +
                            ringtoneUri + "\n ------- \n");
        }
        cursor.close();
        return stringBuffer.toString();
    }
}
