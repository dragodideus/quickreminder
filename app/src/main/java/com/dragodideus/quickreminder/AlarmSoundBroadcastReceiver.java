package com.dragodideus.quickreminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

/**
 * Created by rakifsul on 4/3/2016.
 */
public class AlarmSoundBroadcastReceiver extends BroadcastReceiver {
    public static final String REMINDER_EVENT = "REMINDER_EVENT";

    @Override
    public void onReceive(Context context, Intent intent) {
        //
        ReminderEvent re = (ReminderEvent) intent.getParcelableExtra(REMINDER_EVENT);

        //
        String alarmTitle = re.getTitle();
        String alarmDescription = re.getDescription();

        //
        Intent targetIntent = new Intent(context, ViewReminderEventDetailsActivity.class);
        targetIntent.putExtra(ViewReminderEventDetailsActivity.REMINDER_EVENT, re);

        //
        PendingIntent pi = PendingIntent.getActivity(context, 0, targetIntent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setAutoCancel(true); //remove after click
        builder.setTicker(alarmTitle + " - " + alarmDescription);
        builder.setContentTitle("QuickReminder Notification");
        builder.setContentText(alarmDescription);
        builder.setSmallIcon(R.drawable.ic_action_flag);
        builder.setContentIntent(pi);
        builder.setOngoing(true);
        //builder.setSubText("This is subtext...");   //API level 16
        builder.setNumber(100);
        builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        builder.setLights(Color.BLUE, 3000, 3000);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        //
        NotificationManager mNotificationManager =  (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0,  builder.build());
    }
}
