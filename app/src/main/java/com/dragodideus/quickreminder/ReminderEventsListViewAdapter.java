package com.dragodideus.quickreminder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rakifsul on 3/22/2016.
 */
public class ReminderEventsListViewAdapter extends BaseAdapter {

    public enum ReminderEventsImage {
        UsingAlarm(R.drawable.ic_action_alarm),
        NotUsingAlarm(R.drawable.ic_action_flag);

        ReminderEventsImage (int i)
        {
            this.type = i;
        }

        private int type;

        public int getNumericType()
        {
            return type;
        }
    }

    private ArrayList<ReminderEvent> reminderEvents;
    private Context context;

    public ReminderEventsListViewAdapter(Context context){
        reminderEvents = new ArrayList<ReminderEvent>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return reminderEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return reminderEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //
        View row = layoutInflater.inflate(R.layout.reminder_event_single, parent, false);

        //
        TextView time = (TextView) row.findViewById(R.id.txvTime);
        TextView title = (TextView) row.findViewById(R.id.txvTitle);
        TextView desc = (TextView) row.findViewById(R.id.txvDescription);
        ImageView alarmOrNot = (ImageView) row.findViewById(R.id.imvAlarmOrNot);

        //
        ReminderEvent revt = reminderEvents.get(position);

        //
        time.setText(Utils.formatDisplayTime(revt.getTime()));
        title.setText(revt.getTitle());
        desc.setText(revt.getDescription());

        if(revt.getUsesAlarm()) {
            alarmOrNot.setImageResource(ReminderEventsImage.UsingAlarm.getNumericType());
        }else {
            alarmOrNot.setImageResource(ReminderEventsImage.NotUsingAlarm.getNumericType());
        }

        RelativeLayout layout = (RelativeLayout)row;
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            child.setEnabled(!revt.isBeforeNow());
        }
        return row;
    }

    public void addItem(ReminderEvent revt) {
        reminderEvents.add(revt);
        this.notifyDataSetChanged();
    }

    public void removeItem(int index){
        reminderEvents.remove(index);
        this.notifyDataSetChanged();
    }

    public  void removeAllItems(){
        reminderEvents.clear();
        this.notifyDataSetChanged();
    }
}
