package com.dragodideus.quickreminder;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by rakifsul on 3/25/2016.
 */
public class CreateReminderEventActivity extends AppCompatActivity {

    public static final String SELECTED_DATE = "SELECTED_DATE";
    private static final int RINGTONE_REQUEST_CODE = 5;

    EditText edtTitle;
    EditText edtDescription;
    Button btnTime;
    Button btnChooseTone;
    CheckBox chbUsesAlarm;
    Button btnSave;
    Button btnCancel;

    Uri ringtoneUri;
    Calendar selectedDate = Calendar.getInstance();
    Calendar selectedTime = Calendar.getInstance();
    boolean isEditing = false;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_reminder_event);

        //
        ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (ringtoneUri == null) {
            ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (ringtoneUri == null) {
                ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        //
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                selectedDate.setTime(sdf.parse(extras.getString(SELECTED_DATE)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //
        initViews();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
    {
        if (resultCode == Activity.RESULT_OK && requestCode == RINGTONE_REQUEST_CODE)
        {
            ringtoneUri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (ringtoneUri != null) {
            } else {
                //this.chosenRingtone = null;
                ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                if (ringtoneUri == null) {
                    ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    if (ringtoneUri == null) {
                        ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    }
                }
            }

            //
            Ringtone ringtone = RingtoneManager.getRingtone(this, ringtoneUri);
            String ringtoneTitle = ringtone.getTitle(this);
            btnChooseTone.setText(ringtoneTitle);
        }
    }

    void initViews(){
        //
        edtTitle = (EditText) findViewById(R.id.edtTitle);

        //
        edtDescription = (EditText) findViewById(R.id.edtDescription);

        //
        btnTime = (Button) findViewById(R.id.btnTime);
        if(btnTime != null){
            btnTime.setText(Utils.formatDisplayTime(Calendar.getInstance()));
            btnTime.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnTime){
                        TimePickerDialog tpd = new TimePickerDialog(
                                CreateReminderEventActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                        Calendar cal = Calendar.getInstance();
                                        cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                                        cal.set(Calendar.MINUTE, selectedMinute);
                                        cal.set(Calendar.SECOND, 0);
                                        cal.set(Calendar.MILLISECOND, 0);

                                        btnTime.setText(Utils.formatDisplayTime(cal));
                                        selectedTime = cal;
                                    }
                                },
                                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                                Calendar.getInstance().get(Calendar.MINUTE),
                                true);
                        tpd.show();
                    }
                }
            });
        }

        //
        btnChooseTone = (Button) findViewById(R.id.btnChooseTone);
        if(btnChooseTone != null){
            //
            Ringtone ringtone = RingtoneManager.getRingtone(this, ringtoneUri);
            String ringtoneTitle = ringtone.getTitle(this);
            btnChooseTone.setText(ringtoneTitle);

            //
            btnChooseTone.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnChooseTone) {
                        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                        startActivityForResult(intent, RINGTONE_REQUEST_CODE);
                    }
                }
            });
        }

        //
        chbUsesAlarm = (CheckBox) findViewById(R.id.chbUsesAlarm);
        if(chbUsesAlarm != null){
            btnChooseTone.setEnabled(chbUsesAlarm.isChecked());
            chbUsesAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.getId() == R.id.chbUsesAlarm){
                        if(btnChooseTone != null){
                            btnChooseTone.setEnabled(isChecked);
                        }
                    }
                }
            });
        }

        //
        btnSave = (Button) findViewById(R.id.btnSave);
        if(btnSave != null){
            btnSave.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnSave){
                        //
                        validate();
                        onBackPressed();
                    }
                }
            });
        }

        //
        btnCancel = (Button) findViewById(R.id.btnCancel);
        if(btnCancel != null){
            btnCancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnCancel){
                        //
                        onBackPressed();
                    }
                }
            });
        }
    }

    void validate(){
        //
        DbSQLiteAdapter adp = QuickReminder.getInstance().getDbSQLiteAdapter();

        //
        ReminderEvent reminderEvent = new ReminderEvent();
        reminderEvent.setTitle(edtTitle.getText().toString());
        reminderEvent.setDescription(edtDescription.getText().toString());
        reminderEvent.setDate(selectedDate);
        reminderEvent.setTime(selectedTime);
        reminderEvent.setUsesAlarm(chbUsesAlarm.isChecked());
        reminderEvent.setRingtoneUri(ringtoneUri);

        //
        int id = (int)adp.insertReminderEvent(reminderEvent);
        reminderEvent.setID(id);

        //
        Intent service = new Intent(this, AlarmService.class);
        service.putExtra(AlarmService.REMINDER_EVENT, reminderEvent);
        service.setAction(AlarmService.SET_ALARM);
        startService(service);
    }
}
