package com.dragodideus.quickreminder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by rakifsul on 3/25/2016.
 */
public class Utils {
    //
    private static final String DATE_FORMAT = "yyyyMMdd";
    private static final String DATE_DISPLAY_FORMAT = "MM/dd/yyyy";
    private static final String TIME_FORMAT = "HHmmss";
    private static final String TIME_DISPLAY_FORMAT = "HH:mm";

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static final SimpleDateFormat dateDisplayFormat = new SimpleDateFormat(DATE_DISPLAY_FORMAT);
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
    private static final SimpleDateFormat timeDisplayFormat = new SimpleDateFormat(TIME_DISPLAY_FORMAT);

    private Utils(){}

    public static long dateAsLong(Date date){
        dateFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return Long.parseLong(dateFormat.format(date));
    }

    public static long dateAsLongCalendar(Calendar date){
        return dateAsLong(date.getTime());
    }

    public static Date longAsDate(long l){
        try {
            dateFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            return dateFormat.parse(String.valueOf(l));
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar longAsDateCalendar(long l){
        Calendar date = Calendar.getInstance();
        date.setTime(longAsDate(l));
        return date;
    }

    public static String timeAsString(Date date){
        timeFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return timeFormat.format(date);
    }

    public static String timeAsStringCalendar(Calendar date){
        return timeAsString(date.getTime());
    }

    public static Date stringAsTime(String time){
        try {
            timeFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            return timeFormat.parse(time);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar stringAsTimeCalendar(String str){
        Calendar time = Calendar.getInstance();
        time.setTime(stringAsTime(str));
        return time;
    }

    public static long timeAsLong(Date date){
        timeFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return Long.parseLong(timeFormat.format(date));
    }

    public static long timeAsLongCalendar(Calendar date){
        return timeAsLong(date.getTime());
    }

    public static Date longAsTime(long l){
        try {
            timeFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            return timeFormat.parse(String.valueOf(l));
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar longAsTimeCalendar(long l){
        Calendar time = Calendar.getInstance();
        time.setTime(longAsTime(l));
        return time;
    }

    public static String formatDisplayDate(Date d){
        dateDisplayFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return dateDisplayFormat.format(d);
    }

    public static String formatDisplayDate(Calendar calendar){
        return formatDisplayDate(calendar.getTime());
    }

    public static String formatDisplayTime(Date d){
        timeDisplayFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return timeDisplayFormat.format(d);
    }

    public static String formatDisplayTime(Calendar calendar){
        return formatDisplayTime(calendar.getTime());
    }

    public static int boolToInt(boolean b){
        return (b)? 1 : 0;
    }

    public static boolean intToBool(int i){
        return i != 0;
    }
}
