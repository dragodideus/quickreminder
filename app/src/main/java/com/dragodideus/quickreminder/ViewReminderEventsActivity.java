package com.dragodideus.quickreminder;

/**
 * Created by rakifsul on 3/21/2016.
 */

import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ViewReminderEventsActivity extends AppCompatActivity {

    private static final String TAG = "ViewReminderEvents";
    public static final String SELECTED_DATE = "SELECTED_DATE";

    private Button btnCreateReminderEvent;
    private Button btnBack;
    private ListView lsvReminderEvents;
    private ReminderEventsListViewAdapter reminderEventsListViewAdapter;

    Calendar selectedDate = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reminder_events);

        //
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                selectedDate.setTime(sdf.parse(extras.getString(SELECTED_DATE)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //
        initViews();
    }

    @Override
    public void onResume(){
        super.onResume();

        //
        reminderEventsListViewAdapter.removeAllItems();

        DbSQLiteAdapter adp = QuickReminder.getInstance().getDbSQLiteAdapter();
        ArrayList<ReminderEvent> res = adp.getReminderEventsAtDate(selectedDate);
        if(res == null){
        }
        if(res.size() == 0){
        }
        for(ReminderEvent re : res){
            reminderEventsListViewAdapter.addItem(re);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == R.id.lsvReminderEvents){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle("Choose an Action");
            menu.add(Menu.NONE,0,0,"Edit Event");
            menu.add(Menu.NONE,1,1,"Delete Event");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        if(item.getTitle()=="Edit Event") {
            ReminderEvent re = (ReminderEvent)reminderEventsListViewAdapter.getItem(info.position);
            Intent myIntent = new Intent(this, EditReminderEventActivity.class);
            myIntent.putExtra(EditReminderEventActivity.REMINDER_EVENT, re);
            startActivityForResult(myIntent, 1);
        } else if(item.getTitle()=="Delete Event") {
            ReminderEvent re = (ReminderEvent)reminderEventsListViewAdapter.getItem(info.position);
            if(re != null){
                //
                Intent service = new Intent(this, AlarmService.class);
                service.putExtra(AlarmService.REMINDER_EVENT, re);
                service.setAction(AlarmService.CANCEL_ALARM);
                startService(service);

                //
                reminderEventsListViewAdapter.removeItem(info.position);

                //
                QuickReminder.getInstance().getDbSQLiteAdapter().deleteReminderEvent(re.getID());
            }
        } else {
            return false;
        }
        return true;
    }

    public void initViews(){
        //
        reminderEventsListViewAdapter = new ReminderEventsListViewAdapter(this);
        lsvReminderEvents = (ListView) findViewById(R.id.lsvReminderEvents);
        if(lsvReminderEvents != null){
            lsvReminderEvents.setAdapter(reminderEventsListViewAdapter);
            lsvReminderEvents.setClickable(true);
            lsvReminderEvents.setLongClickable(true);
            registerForContextMenu(lsvReminderEvents);

            lsvReminderEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ReminderEvent re = (ReminderEvent)reminderEventsListViewAdapter.getItem(position);
                    Intent targetIntent = new Intent(ViewReminderEventsActivity.this, ViewReminderEventDetailsActivity.class);
                    targetIntent.putExtra(ViewReminderEventDetailsActivity.REMINDER_EVENT, re);
                    startActivityForResult(targetIntent, 0);
                }
            });

            /*
            lsvReminderEvents.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(ViewReminderEventsActivity.this, "long click", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            */
        }

        //
        btnCreateReminderEvent = (Button) findViewById(R.id.btnCreateReminderEvent);
        if(btnCreateReminderEvent != null) {
            btnCreateReminderEvent.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnCreateReminderEvent){
                        Intent myIntent = new Intent(v.getContext(), CreateReminderEventActivity.class);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        myIntent.putExtra(CreateReminderEventActivity.SELECTED_DATE,sdf.format(selectedDate.getTime()));
                        startActivityForResult(myIntent, 0);
                    }
                }
            });
        }

        //
        btnBack = (Button) findViewById(R.id.btnBack);
        if(btnBack != null){
            btnBack.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnBack){
                        onBackPressed();
                    }
                }
            });
        }
    }

    void howToInserListViewItem(){
        reminderEventsListViewAdapter.addItem(
                new ReminderEvent(
                        0,
                        "Wake up!",
                        "Wakeupnow",
                        Calendar.getInstance(),
                        Calendar.getInstance(),
                        true,
                        RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
                )
        );
    }
}
