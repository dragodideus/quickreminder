package com.dragodideus.quickreminder;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by rakifsul on 4/1/2016.
 */
public class AlarmSoundActivity extends AppCompatActivity {
    public static final String REMINDER_EVENT = "REMINDER_EVENT";

    private MediaPlayer player;
    Vibrator vibe;
    boolean isVibrating = false;
    final Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_alarm_sound);

        //
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        isVibrating = true;

        //
        ReminderEvent re = (ReminderEvent) getIntent().getParcelableExtra(REMINDER_EVENT);

        //
        TextView txvAlarmSoundTitle = (TextView) findViewById(R.id.txvAlarmSoundTitle);
        if(txvAlarmSoundTitle != null){
            txvAlarmSoundTitle.setText(re.getTitle());
        }

        //
        TextView txvAlarmSoundTime = (TextView) findViewById(R.id.txvAlarmSoundTime);
        if(txvAlarmSoundTime != null){
            txvAlarmSoundTime.setText(Utils.formatDisplayTime(re.getTime()));
        }

        //
        TextView txvAlarmSoundDescription = (TextView) findViewById(R.id.txvAlarmSoundDescription);
        if(txvAlarmSoundDescription != null){
            txvAlarmSoundDescription.setText(re.getDescription());
            txvAlarmSoundDescription.setMovementMethod(new ScrollingMovementMethod());
        }

        //
        Button stop = (Button) findViewById(R.id.btnAlarmSoundStop);
        if(stop != null) {
            stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    player.stop();
                    vibe.cancel();
                    isVibrating = false;
                    onBackPressed();
                }
            });
        }

        play(this, re.getRingtoneUri());

    }

    private void play(Context context, Uri alert) {
        player = new MediaPlayer();
        try {
            player.setDataSource(context, alert);
            final AudioManager audio = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audio.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                player.setAudioStreamType(AudioManager.STREAM_ALARM);
                player.setLooping(true);
                player.prepare();
                player.start();

                //
                //final Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                //vibe.vibrate(80000); //time in ms

                //
                new CountDownTimer(30 * 1000,1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        if(isVibrating == true){
                            vibe.vibrate(800);
                        }
                    }

                    @Override
                    public void onFinish() {
                        if(player.isPlaying()) {
                            player.stop();
                        }
                        vibe.cancel();
                        isVibrating = false;
                    }
                }.start();
            }
        } catch (IOException e) {
            Log.e("Error....","Check code...");
        }
    }
}
