package com.dragodideus.quickreminder;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by rakifsul on 4/5/2016.
 */
public class EditReminderEventActivity extends AppCompatActivity {

    public static final String REMINDER_EVENT = "REMINDER_EVENT";
    private static final int RINGTONE_REQUEST_CODE = 5;

    EditText edtTitle;
    EditText edtDescription;
    Button btnTime;
    Button btnChooseTone;
    CheckBox chbUsesAlarm;
    Button btnSave;
    Button btnCancel;

    Uri ringtoneUri;
    Calendar selectedDate = Calendar.getInstance();
    Calendar selectedTime = Calendar.getInstance();

    ReminderEvent tempReminderEvent;
    boolean oldUsesAlarm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_reminder_event);

        //
        ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (ringtoneUri == null) {
            ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (ringtoneUri == null) {
                ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        //
        tempReminderEvent = getIntent().getParcelableExtra(REMINDER_EVENT);
        if(tempReminderEvent != null){
            oldUsesAlarm = tempReminderEvent.getUsesAlarm();
        }
        //
        initViews();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
    {
        if (resultCode == Activity.RESULT_OK && requestCode == RINGTONE_REQUEST_CODE)
        {
            ringtoneUri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (ringtoneUri != null) {
            } else {
                ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                if (ringtoneUri == null) {
                    ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    if (ringtoneUri == null) {
                        ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    }
                }
            }

            //
            Ringtone ringtone = RingtoneManager.getRingtone(this, ringtoneUri);
            String ringtoneTitle = ringtone.getTitle(this);
            btnChooseTone.setText(ringtoneTitle);
        }
    }

    void initViews(){
        selectedDate = tempReminderEvent.getDate();
        selectedTime = tempReminderEvent.getTime();
        ringtoneUri = tempReminderEvent.getRingtoneUri();
        //
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        if(edtTitle != null){
            edtTitle.setText(tempReminderEvent.getTitle());
        }
        //
        edtDescription = (EditText) findViewById(R.id.edtDescription);
        if(edtDescription != null){
            edtDescription.setText(tempReminderEvent.getDescription());
        }

        //
        btnTime = (Button) findViewById(R.id.btnTime);
        if(btnTime != null){
            btnTime.setText(Utils.formatDisplayTime(tempReminderEvent.getTime()));
            btnTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnTime){
                        TimePickerDialog tpd = new TimePickerDialog(
                                EditReminderEventActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                        Calendar cal = Calendar.getInstance();
                                        cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                                        cal.set(Calendar.MINUTE, selectedMinute);
                                        cal.set(Calendar.SECOND, 0);
                                        cal.set(Calendar.MILLISECOND, 0);

                                        btnTime.setText(Utils.formatDisplayTime(cal));
                                        selectedTime = cal;
                                    }
                                },
                                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                                Calendar.getInstance().get(Calendar.MINUTE),
                                true);
                        tpd.show();
                    }
                }
            });
        }

        //
        btnChooseTone = (Button) findViewById(R.id.btnChooseTone);
        if(btnChooseTone != null){
            Ringtone ringtone = RingtoneManager.getRingtone(this, ringtoneUri);
            String ringtoneTitle = ringtone.getTitle(this);
            btnChooseTone.setText(ringtoneTitle);
            btnChooseTone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnChooseTone) {
                        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                        startActivityForResult(intent, RINGTONE_REQUEST_CODE);
                    }
                }
            });
        }

        //
        chbUsesAlarm = (CheckBox) findViewById(R.id.chbUsesAlarm);
        if(chbUsesAlarm != null){
            chbUsesAlarm.setChecked(tempReminderEvent.getUsesAlarm());
            btnChooseTone.setEnabled(chbUsesAlarm.isChecked());
            chbUsesAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.getId() == R.id.chbUsesAlarm){
                        if(btnChooseTone != null){
                            btnChooseTone.setEnabled(isChecked);
                        }
                    }
                }
            });
        }

        //
        btnSave = (Button) findViewById(R.id.btnSave);
        if(btnSave != null){
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnSave){
                        //
                        validate();
                        onBackPressed();
                    }
                }
            });
        }

        //
        btnCancel = (Button) findViewById(R.id.btnCancel);
        if(btnCancel != null){
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnCancel){
                        //
                        onBackPressed();
                    }
                }
            });
        }
    }

    void validate(){
        //
        DbSQLiteAdapter adp = QuickReminder.getInstance().getDbSQLiteAdapter();

        //
        tempReminderEvent.setTitle(edtTitle.getText().toString());
        tempReminderEvent.setDescription(edtDescription.getText().toString());
        tempReminderEvent.setDate(selectedDate);
        tempReminderEvent.setTime(selectedTime);
        tempReminderEvent.setUsesAlarm(chbUsesAlarm.isChecked());
        tempReminderEvent.setRingtoneUri(ringtoneUri);

        //
        adp.updateReminderEvent(tempReminderEvent);

        //
        Intent service = new Intent(this, AlarmService.class);
        service.putExtra(AlarmService.REMINDER_EVENT, tempReminderEvent);
        service.putExtra(AlarmService.OLD_USES_ALARM, oldUsesAlarm);
        service.setAction(AlarmService.UPDATE_ALARM);
        startService(service);
    }
}
