package com.dragodideus.quickreminder;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.text.SimpleDateFormat;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;

import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.CalendarPickerView.SelectionMode;
import com.startapp.android.publish.StartAppSDK;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    //
    private CalendarPickerView cpvCalendar;
    private Button btnCreateViewEvents;
    private Button btnGoToDate;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, "203844317", true);
        setContentView(R.layout.activity_main);

        //
        initViews();

        //
        Intent service = new Intent(this, AlarmService.class);
        service.putParcelableArrayListExtra(
                AlarmService.REMINDER_EVENT_ARRAYLIST,
                QuickReminder.getInstance().getDbSQLiteAdapter().getAllReminderEvents());
        service.setAction(AlarmService.POPULATE_ALARM);
        startService(service);
    }

    @Override
    public void onResume() {
        super.onResume();

        //
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        //
        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        //
        highlightCalendar(lastYear, nextYear);
    }

    void initViews(){
        //
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        //
        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        //
        cpvCalendar = (CalendarPickerView) findViewById(R.id.cpvCalendar);
        cpvCalendar.init(lastYear.getTime(), nextYear.getTime())
                .inMode(SelectionMode.SINGLE)
                .withSelectedDate(new Date());

        //
        highlightCalendar(lastYear, nextYear);

        //
        btnCreateViewEvents = (Button) findViewById(R.id.btnCreateViewReminderEvents);
        if(btnCreateViewEvents != null) {
            btnCreateViewEvents.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() == R.id.btnCreateViewReminderEvents) {
                        Intent myIntent = new Intent(v.getContext(), ViewReminderEventsActivity.class);
                        //
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        myIntent.putExtra(ViewReminderEventsActivity.SELECTED_DATE,sdf.format(cpvCalendar.getSelectedDate()));
                        //
                        startActivityForResult(myIntent, 0);
                    }
                }
            });
        }

        //
        btnGoToDate = (Button) findViewById(R.id.btnGoToDate);
        if(btnGoToDate != null){
            btnGoToDate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == R.id.btnGoToDate){
                        LimitedDatePickerDialog dpd = new LimitedDatePickerDialog(
                                MainActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        Calendar cal = Calendar.getInstance();
                                        cal.set(year, monthOfYear, dayOfMonth);
                                        cpvCalendar.selectDate(cal.getTime(),true);
                                    }
                                },
                                Calendar.getInstance().get(Calendar.YEAR),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                        //
                        final Calendar maxDate = Calendar.getInstance();
                        maxDate.add(Calendar.YEAR, 1);

                        //
                        final Calendar minDate = Calendar.getInstance();
                        minDate.add(Calendar.YEAR, -1);

                        maxDate.add(Calendar.DAY_OF_MONTH, -1);
                        dpd.setRange(minDate, maxDate);
                        dpd.show();
                    }
                }
            });
        }
    }

    void highlightCalendar(Calendar lastYear, Calendar nextYear){
        cpvCalendar.clearHighlightedDates();

        //
        ArrayList<ReminderEvent> res = QuickReminder.getInstance().getDbSQLiteAdapter().getAllReminderEvents();
        ArrayList<Date> dates = new ArrayList<Date>();
        for(ReminderEvent re : res){
            Date reDate = re.getDate().getTime();
            if(reDate.after(lastYear.getTime()) && reDate.before(nextYear.getTime())) {
                dates.add(reDate);
            }
        }
        cpvCalendar.highlightDates(dates);
    }

    void howToHighlightCalendar(){
        //
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date blueDate = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();

        //
        ArrayList<Date> dates = new ArrayList<Date>();
        dates.add(blueDate);
        dates.add(greenDate);

        //
        cpvCalendar.highlightDates(dates);
    }
}
