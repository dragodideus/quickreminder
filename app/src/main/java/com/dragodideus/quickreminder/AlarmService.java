package com.dragodideus.quickreminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by rakifsul on 4/1/2016.
 */
public class AlarmService extends IntentService {

    private static final String TAG = "AlarmService";
    public static final String REMINDER_EVENT = "REMINDER_EVENT";
    public static final String REMINDER_EVENT_ARRAYLIST = "REMINDER_EVENT_ARRAYLIST";
    public static final String OLD_USES_ALARM = "OLD_USES_ALARM";

    public static final String SET_ALARM = "SET_ALARM";
    public static final String POPULATE_ALARM = "POPULATE_ALARM";
    public static final String CANCEL_ALARM = "CANCEL_ALARM";
    public static final String UPDATE_ALARM = "UPDATE_ALARM";

    private IntentFilter matcher;

    public AlarmService(){
        super(TAG);
        matcher = new IntentFilter();
        matcher.addAction(SET_ALARM);
        matcher.addAction(POPULATE_ALARM);
        matcher.addAction(CANCEL_ALARM);
        matcher.addAction(UPDATE_ALARM);
        //Log.d(TAG,"!!!!!!!!!!!!!!!!AlarmService-constructor!!!!!!!!!!!!!!!!!");
    }

    @Override
    public void onCreate (){
        super.onCreate();
        //Log.d(TAG,"!!!!!!!!!!!!!!!!AlarmService-onCreate!!!!!!!!!!!!!!!!!");
    }

    @Override
    public void onDestroy (){
        super.onDestroy();
        //Log.d(TAG,"!!!!!!!!!!!!!!!!AlarmService-onDestroy!!!!!!!!!!!!!!!!!");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //Log.d(TAG,"!!!!!!!!!!!!!!!!AlarmService-onHandleIntent-before alarm!!!!!!!!!!!!!!!!!");

        String action = intent.getAction();

        if (matcher.matchAction(action)) {
            if (SET_ALARM.equals(action)) {
                ReminderEvent re = (ReminderEvent) intent.getParcelableExtra(REMINDER_EVENT);
                if(re != null){
                    setAlarm(re);
                }
            }

            if (POPULATE_ALARM.equals(action)) {
                ArrayList<ReminderEvent> res = intent.getParcelableArrayListExtra(REMINDER_EVENT_ARRAYLIST);
                if(res != null){
                    populateAlarm(res);
                }
            }

            if (CANCEL_ALARM.equals(action)) {
                ReminderEvent re = (ReminderEvent) intent.getParcelableExtra(REMINDER_EVENT);
                if(re != null){
                    cancelAlarm(re);
                }
            }

            if (UPDATE_ALARM.equals(action)) {
                ReminderEvent re = (ReminderEvent) intent.getParcelableExtra(REMINDER_EVENT);
                boolean oldUsesAlarm = intent.getBooleanExtra(OLD_USES_ALARM, false);
                if(re != null){
                    updateAlarm(re, oldUsesAlarm);
                }
            }
        }

        //Log.d(TAG,"!!!!!!!!!!!!!!!!AlarmService-onHandleIntent-after alarm!!!!!!!!!!!!!!!!!");
    }

    void updateAlarm(ReminderEvent reminderEvent, boolean oldUsesAlarm){
        AlarmManager mgrAlarm = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        ReminderEvent re = reminderEvent;
        if(re == null){
            return;
        }

        if(re.getUsesAlarm() == oldUsesAlarm) {
            if (!re.getUsesAlarm()) {
                //
                Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
                intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (re.isBeforeNow()) {
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            } else {
                //
                Intent intent = new Intent(this, AlarmSoundActivity.class);
                intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (re.isBeforeNow()) {
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            }
        } else {
            //
            if (!oldUsesAlarm) {
                //
                Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
                intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                mgrAlarm.cancel(pendingIntent);

            } else {
                //
                Intent intent = new Intent(this, AlarmSoundActivity.class);
                intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                mgrAlarm.cancel(pendingIntent);
            }

            //
            if (!re.getUsesAlarm()) {
                //
                Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
                intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (re.isBeforeNow()) {
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            } else {
                //
                Intent intent = new Intent(this, AlarmSoundActivity.class);
                intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (re.isBeforeNow()) {
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            }
        }
    }

    void setAlarm(ReminderEvent reminderEvent){
        AlarmManager mgrAlarm = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        ReminderEvent re = reminderEvent;
        if(re == null){
            return;
        }

        if(!re.getUsesAlarm()) {
            //
            Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
            intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

            //
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if(re.isBeforeNow()){
                mgrAlarm.cancel(pendingIntent);
            } else {
                mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                        re.getDateTime().getTimeInMillis(),
                        pendingIntent);
            }
        } else {
            //
            Intent intent = new Intent(this, AlarmSoundActivity.class);
            intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

            //
            PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if(re.isBeforeNow()){
                mgrAlarm.cancel(pendingIntent);
            } else {
                mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                        re.getDateTime().getTimeInMillis(),
                        pendingIntent);
            }
        }
    }

    void cancelAlarm(ReminderEvent reminderEvent){
        //Log.d(TAG,"cancelAlarm");
        AlarmManager mgrAlarm = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        ReminderEvent re = reminderEvent;
        if(re == null){
            //Log.d(TAG,"cancelAlarm null");
            return;
        }

        if(!re.getUsesAlarm()) {
            //
            Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
            intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

            //
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

            //
            mgrAlarm.cancel(pendingIntent);
            //Log.d(TAG,"cancelAlarm pending intent not using alarm");

        } else {
            //
            Intent intent = new Intent(this, AlarmSoundActivity.class);
            intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

            //
            PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

            //
            mgrAlarm.cancel(pendingIntent);
            //Log.d(TAG,"cancelAlarm pending intent using alarm");
        }
    }

    void populateAlarm(ArrayList<ReminderEvent> res){
        AlarmManager mgrAlarm = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);

        if(res == null){
            return;
        }

        for(ReminderEvent re : res){
            //
            if(!re.getUsesAlarm()){
                //
                Intent intent = new Intent(this, AlarmSoundBroadcastReceiver.class);
                intent.putExtra(AlarmSoundBroadcastReceiver.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if(re.isBeforeNow()){
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            }else {
                //
                Intent intent = new Intent(this, AlarmSoundActivity.class);
                intent.putExtra(AlarmSoundActivity.REMINDER_EVENT, re);

                //
                PendingIntent pendingIntent = PendingIntent.getActivity(this, re.getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if(re.isBeforeNow()){
                    mgrAlarm.cancel(pendingIntent);
                } else {
                    mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                            re.getDateTime().getTimeInMillis(),
                            pendingIntent);
                }
            }
        }
    }
}
