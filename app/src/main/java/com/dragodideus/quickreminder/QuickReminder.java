package com.dragodideus.quickreminder;

import android.app.Application;
import android.content.res.Configuration;

/**
 * Created by rakifsul on 3/27/2016.
 */
public class QuickReminder extends Application {

    private DbSQLiteAdapter dbSQLiteAdapter;

    //
    private static QuickReminder singleton;
    public static QuickReminder getInstance(){
        return singleton;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        //
        dbSQLiteAdapter = new DbSQLiteAdapter(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public DbSQLiteAdapter getDbSQLiteAdapter(){
        return dbSQLiteAdapter;
    }
}
