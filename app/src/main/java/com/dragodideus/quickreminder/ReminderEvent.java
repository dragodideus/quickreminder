package com.dragodideus.quickreminder;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by rakifsul on 3/22/2016.
 */
public class ReminderEvent implements Parcelable {
    private int id;
    private String title;
    private String description;
    private Calendar date = Calendar.getInstance();
    private Calendar time = Calendar.getInstance();
    private boolean usesAlarm;
    private Uri ringtoneUri;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeSerializable(date);
        dest.writeSerializable(time);
        dest.writeByte((byte) (usesAlarm ? 1 : 0));
        dest.writeString(ringtoneUri.toString());
        //dest.writeParcelable(ringtoneUri,flags);
    }

    public static final Parcelable.Creator<ReminderEvent> CREATOR = new Parcelable.Creator<ReminderEvent>() {
        public ReminderEvent createFromParcel(Parcel in) {
            return new ReminderEvent(in);
        }

        public ReminderEvent[] newArray(int size) {
            return new ReminderEvent[size];
        }
    };

    public ReminderEvent(){
    }

    private ReminderEvent(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        date = (Calendar)in.readSerializable();
        time = (Calendar)in.readSerializable();
        usesAlarm = in.readByte() != 0;
        ringtoneUri = Uri.parse(in.readString());
        //ringtoneUri = in.readParcelable();
    }

    public ReminderEvent(int id,
                         String title,
                         String description,
                         Calendar date,
                         Calendar time,
                         boolean usesAlarm,
                         Uri ringtoneUri) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.time = time;
        this.usesAlarm = usesAlarm;
        this.ringtoneUri = ringtoneUri;
    }

    public int getID(){
        return id;
    }

    public void setID(int id){
        this.id = id;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public Calendar getDate(){
        return date;
    }

    public void setDate(Calendar date){
        this.date = date;
    }

    public Calendar getTime(){
        return time;
    }

    public void setTime(Calendar time){
        this.time = time;
    }

    public boolean getUsesAlarm(){
        return usesAlarm;
    }

    public void setUsesAlarm(boolean usesAlarm){
        this.usesAlarm = usesAlarm;
    }

    public Uri getRingtoneUri(){
        return ringtoneUri;
    }

    public void setRingtoneUri(Uri ringtoneUri){
        this.ringtoneUri = ringtoneUri;
    }

    public Calendar getDateTime(){
        Calendar calendarA = getDate();

        Calendar calendarB = getTime();

        calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
        calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
        //calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
        //calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));

        return calendarA;
    }

    public boolean isBeforeNow(){
        Calendar calendarA = getDate();

        Calendar calendarB = getTime();

        calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
        calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
        calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
        calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));

        Date result = calendarA.getTime();

        if(result.before(new Date())){
            return true;
        }
        return false;
    }
}
