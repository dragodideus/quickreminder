package com.dragodideus.quickreminder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rakifsul on 4/5/2016.
 */
public class ViewReminderEventDetailsActivity extends AppCompatActivity {
    public static final String REMINDER_EVENT = "REMINDER_EVENT";

    ReminderEvent selectedReminderEvent;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reminder_event_details);

        //
        selectedReminderEvent = (ReminderEvent) getIntent().getParcelableExtra(REMINDER_EVENT);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Toast.makeText(this, selectedReminderEvent.getTitle(), Toast.LENGTH_SHORT).show();

        final ReminderEvent re = QuickReminder.getInstance().getDbSQLiteAdapter().getReminderEvent(selectedReminderEvent.getID());

        //
        TextView txvAlarmSoundTitle = (TextView) findViewById(R.id.txvReminderEventDetailTitle);
        if(txvAlarmSoundTitle != null){
            txvAlarmSoundTitle.setText(re.getTitle());
        }

        //
        TextView txvAlarmSoundTime = (TextView) findViewById(R.id.txvReminderEventDetailTime);
        if(txvAlarmSoundTime != null){
            txvAlarmSoundTime.setText(Utils.formatDisplayTime(re.getTime()));
        }

        //
        TextView txvAlarmSoundDescription = (TextView) findViewById(R.id.txvReminderEventDetailDescription);
        if(txvAlarmSoundDescription != null){
            txvAlarmSoundDescription.setText(re.getDescription());
        }

        //
        TextView txvReminderEventDetailType = (TextView) findViewById(R.id.txvReminderEventDetaiType);
        if(txvReminderEventDetailType != null){
            txvReminderEventDetailType.setText(re.getUsesAlarm() == true? "Alarm" : "Notification");
        }

        //
        Button btnReminderEventDetailEdit = (Button) findViewById(R.id.btnReminderEventDetailEdit);
        if(btnReminderEventDetailEdit != null){
            btnReminderEventDetailEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(ViewReminderEventDetailsActivity.this, EditReminderEventActivity.class);
                    myIntent.putExtra(EditReminderEventActivity.REMINDER_EVENT, re);
                    startActivityForResult(myIntent, 1);
                }
            });
        }

        //
        Button btnReminderEventDetailCancel = (Button) findViewById(R.id.btnReminderEventDetailCancel);
        if(btnReminderEventDetailCancel != null){
            btnReminderEventDetailCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }
}
