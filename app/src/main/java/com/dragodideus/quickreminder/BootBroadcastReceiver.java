package com.dragodideus.quickreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by rakifsul on 3/27/2016.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent service = new Intent(context, AlarmService.class);
            service.putParcelableArrayListExtra(
                    AlarmService.REMINDER_EVENT_ARRAYLIST,
                    QuickReminder.getInstance().getDbSQLiteAdapter().getAllReminderEvents());
            service.setAction(AlarmService.POPULATE_ALARM);
            context.startService(service);
        }
    }
}
